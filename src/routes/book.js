const express = require("express");
const path = require("path");

const book = express.Router();
const BookService = require("../services/bookService");

const bookService = new BookService(
  path.resolve(__dirname, "../data/books.json")
);

module.exports = () => {
  book
    .post("/newBook", (req, res) => {
      // Создание новой книги
      if (
        req.body.isbn &&
        req.body.name &&
        req.body.author &&
        req.body.genre &&
        req.body.shelf
      ) {
        bookService.newBook(req.body);
        res.status(200).json(bookService.getBook(req.body.id));
        // .sendStatus(200); Работает!!!
        // .end; Не работает (((
      } else res.status(400).send("The required parameters was not provided.");
    })
    .put("/moveBook", (req, res) => {
      // Перемещение книги
      if (req.body.id && req.body.newShelf) {
        if (bookService.moveBook(req.body.id, req.body.newShelf) !== undefined)
          res.status(200).json(bookService.getBook(req.body.id));
        else res.status(404).send("There is nothing with the provided ID");
      } else res.status(400).send("The required parameters was not provided.");
    })
    .get("/book/:idBook", (req, res) => {
      // Получение книги по id
      console.log(req.params.idBook);
      if (req.body.id) {
        const reqBook = bookService.getBook(req.params.idBook);
        console.log(reqBook);
        // if (Object.keys(reqBook).length !== 0)
        if (reqBook !== undefined) {
          res.status(200).json(reqBook);
        } else res.status(404).send("This book does not exist");
      } else res.status(400).send("The required parameter was not provided.");
    })
    .get("/search", (req, res) => {
      // Поиск книг по параметрам
      if (req.body)
        if (Object.keys(req.body).length !== 0)
          res.status(200).json(bookService.searchBooks(req.body));
        else {
          if (req.query) {
            res.status(200).json(bookService.searchBooks(req.query));
          } else
            res.status(400).send("The required parameter was not provided.");
        }
    })

    .put("/take", (req, res) => {
      // Взятие книги читателем
      if (req.body.id && req.body.userId && req.body.period) {
        const reqBook = bookService.getBook(req.body.id);
        if (reqBook)
          if (reqBook.userId == 0)
            res
              .status(200)
              .json(
                bookService.takeBook(
                  req.body.id,
                  req.body.userId,
                  req.body.period
                )
              );
          else res.status(404).send("This book already taken.");
        else res.status(404).send("There is nothing with the provided ID.");
      } else res.status(400).send("The required parameter was not provided.");
    })
    .put("/reserve", (req, res) => {
      // Резервирование книги читателем,
      if (req.body.id && req.body.userId) {
        const reqBook = bookService.getBook(req.body.id);
        if (reqBook)
          if (reqBook.userId != 0)
            if (reqBook.reservedByUser == 0 || req.body.remove == 1)
              // или отмена резервирования
              res
                .status(200)
                .json(
                  bookService.reserveBook(
                    req.body.id,
                    req.body.userId,
                    req.body.remove
                  )
                );
            else res.status(404).send("This book already reserved.");
          else res.status(404).send("This book not taken yet.");
        else res.status(404).send("There is nothing with the provided ID.");
      } else if (req.body.userId)
        // или список зарезервированных книг
        res.status(200).json(bookService.listReservedBook(req.body.userId));
      else res.status(400).send("The required parameter was not provided.");
    })

    .get("/shelf", (req, res) => {
      // Список книг на полке
      if (req.body.shelf)
        res.status(200).json(bookService.searchBooks(req.body));
      else
        res
          .status(400)
          .send("The required parameter 'shelf' was not provided.");
    });

  return book;
};

module.exports.bookService = bookService;
