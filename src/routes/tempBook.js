const express = require("express");

const tempBook = express.Router();

const tempBooks = [
  {
    id: 1,
    isbn: "qwer1234",
    name: "War and Peace",
    author: "Tolstoy",
    genre: "drama",
    shelf: 1,
    userId: 0,
    returnDate: "",
    reservedByUser: 0,
  },
];

module.exports = () => {
  tempBook
    .post("/", (req, res) => {
      console.log(req.body.name);
      if (req.body.name) {
        tempBooks.push(req.body);
      }
      res.json(tempBooks);
    })
    .get("/", (req, res) => {
      console.log(tempBooks);
      res.json(tempBooks);
    }); // или getBooks
  // .put("/book", bookService.changeBook);

  return tempBook;
};
