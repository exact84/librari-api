const fs = require("fs");

class BookService {
  constructor(fileName) {
    this.books = JSON.parse(fs.readFileSync(fileName, "utf8"));
    this.fileName = fileName;
  }

  async getBook(idBook) {
    // console.log(this.books[1], idBook);
    let book = await this.books.find((item) => {
      return item.id == idBook;
    });
    // console.table(book);
    if (book) return book;
    // else return {};
  }

  async newBook(book) {
    var maxId = 1;
    for (let elem of this.books) if (elem.id > maxId) maxId = elem.id;
    book.id = maxId + 1;
    book.userId = 0;
    book.returnDate = "";
    book.reservedByUser = 0;
    // console.log(book);
    await this.books.unshift(book);
    // console.table(this.books);
    this.save();
    return book;
  }

  async moveBook(idBook, newShelf) {
    const book = this.getBook(idBook);
    if (book) {
      let bookIndex = this.books.findIndex((item) => {
        return item.id == idBook;
      });
      this.books[bookIndex].shelf = newShelf;
      // console.table(this.books[bookIndex]);
      book.shelf = newShelf;
      this.save();
      return book;
    }
  }

  async searchBooks(search) {
    // console.table(this.books);
    // console.log(search);
    let resBooks = {};
    resBooks = await this.books.filter((item) => {
      return (
        item.isbn == search.isbn ||
        item.name == search.name ||
        item.author == search.author ||
        item.genre == search.genre ||
        item.shelf == search.shelf
      );
    });
    return resBooks;
  }

  async takeBook(id, userId, period) {
    const book = this.getBook(id);
    if (book) {
      let bookIndex = this.books.findIndex((item) => {
        return item.id == id;
      });
      this.books[bookIndex].userId = userId;
      let retDate = new Date();
      retDate.setDate(retDate.getDate() + period);
      this.books[bookIndex].returnDate = retDate.toISOString().slice(0, 10);
      // console.table(this.books[bookIndex]);
      this.save();
      // console.log(book);
      return book;
    }
  }

  async reserveBook(id, userId, remove) {
    const book = this.getBook(id);
    if (book) {
      let bookIndex = this.books.findIndex((item) => {
        return item.id == id;
      });
      this.books[bookIndex].reservedByUser = userId;
      if (remove == 1) this.books[bookIndex].reservedByUser = 0;
      // console.table(this.books[bookIndex]);
      this.save();
      // console.log(book);
      return book;
    }
  }

  async listReservedBook(userId) {
    let resBooks = await this.books.filter((item) => {
      return item.reservedByUser == userId;
    });
    return resBooks;
  }

  save() {
    return new Promise((resolve, reject) => {
      fs.writeFile(
        this.fileName,
        JSON.stringify(this.books),
        "utf8",
        (error) => {
          if (error) reject(error);
          resolve();
        }
      );
    });
  }
}

module.exports = BookService;
