const express = require("express");
const bodyParser = require("body-parser");

const bookRoute = require("./routes/book");
const tempBook = require("./routes/tempBook");

const app = express();
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set("view engine", "ejs");
// app.set('views', path.join(__dirname, './views'));
// app.locals.siteName = 'Library';
// app.use(express.static(path.join(__dirname, './static')));

app.use(express.json());

const port = 3000;

// app.get("/", (req, res) => {
//   res.sendFile(__dirname + "/index.html");
// });

// app.use("/Tbook", tempBook()); Для теста
app.use("/", bookRoute());

app.listen(port, () => {
  console.log("Express server is listening on http://localhost:" + port);
});
